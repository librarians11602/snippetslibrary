import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by user on 07.05.2017.
 */
public class Client {
    private static FileWorker fileWorker;
    private static Scanner sc;

    // C:\Users\Users\IdeaProjects\SnippetsLibrary\src\Library\Algorithm\sorts\HeapSort.java
    public static void main(String[] args) {
        int choice;
        sc = new Scanner(System.in);
        boolean stop = false;
        fileWorker = new FileWorker();
        while (!stop) {
            fileWorker.setStandard();
            try {
                System.out.println("==================================================================================");
                System.out.println("1: Найти сниппет \n2: Добавить файл \n3: Остановить");
                System.out.print("Выбор: ");
                choice = sc.nextInt();
                System.out.println("==================================================================================");
                switch (choice) {
                    case 1:
                        getFile();
                        break;
                    case 2:
                        // TODO выписывание текста класса при добавлении. Зачем это?
                        addFile();
                        break;
                    case 3:
                        stop = true;
                        System.out.println("Программа остановлена.");
                        break;
                    default:
                        throw new IllegalStateException();
                }
            } catch (IllegalStateException ex) {
                System.out.println("Значение введено неправильно.\nВведите значение заново.");
            }

        }
    }

    public static void getFile() {
        fileWorker.workWithFile(); // Ищем файл(если не знаем название) и работаем с ним
    }
    public static void addFile() {
        System.out.println("Введите путь добавляемого файла: ");
        sc.nextLine();
        String path = sc.nextLine();
        try {
            if (new File(path).exists()) {
                final boolean[] flagPath = {false};
                path = path.replace("\\", ".`.");
                if (path.matches("[A-Z]{1,}(:).*")) {
                    path = Arrays.stream(path.split("(.`.)"))
                            .filter(n -> {
                                // TODO Добавление файла только когда он находится в SRC
                                if (n.equals("src") && !flagPath[0]) {
                                    flagPath[0] = true;
                                }
                                return flagPath[0];
                            })
                            .reduce("", (acc, n) -> acc + n + "/");
                }
                fileWorker.addFile(new File(path));
            } else throw new FileNotFoundException();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден.");
        }
    }
}

