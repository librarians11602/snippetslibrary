public class QuickSort<T extends Comparable<T>> {

    private static final Random RAND ; 
    public static PIVOT_TYPE type ; 

    private QuickSort() { }

    public static <T extends Comparable<T>> T[] sort(PIVOT_TYPE pivotType, T[] unsorted) {}

    private static <T extends Comparable<T>> void sort(int index, int start, int finish, T[] unsorted) {}

    private static final int getRandom(int length) {}

    private static <T extends Comparable<T>> void swap(int index1, int index2, T[] unsorted) {}

}