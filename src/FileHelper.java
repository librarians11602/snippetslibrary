import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Created by user on 13.05.2017.
 */
public class FileHelper implements Runnable {
    private static final String CONSTRUCTOR_MARK = "Constructor_";
    private static final String EXTENSION_TXT = ".txt";
    private final File directory;
    private int choice;
    private Constructor[] constructors;
    private Method[] methodsNames;
    private boolean flag2;
    private int count;
    private String path;
    private File file;
    private String methods;

    public FileHelper(int choice, Constructor[] constructors, Method[] methodsNames,
                      boolean flag2,int count, String path,String methods,File file,File directory){
        this.choice = choice;
        this.constructors = constructors.clone();
        this.methodsNames = methodsNames.clone();
        this.flag2 = flag2;
        this.count = count;
        this.path = path;
        this.file = file;
        this.methods = methods;
        this.directory = directory;
    }

    @Override
    public void run() {
        String resMethod = this.getConcreteMethod(methods, choice); // ищет конкретный метод по сигнатуре
        //System.out.println(resMethod);    // вывод содержимого метода
        String methodName = ""; // название(не сигнатура) нашего метода
        //нужна первая строка метода
        String[] met = Arrays.stream(resMethod.split("\\n")).filter(n -> !n.equals("")).toArray(String[]::new);
        for (int j = 0; j < count && !this.flag2; j++) {
            //смотрим на совпадение с названием метода или конструктора(рефлексия)
            if (j < methodsNames.length && met[0].matches(".*(" + methodsNames[j].getName() + ")\\s*[(].*")) {
                this.flag2 = true;
                methodName = methodsNames[j].getName();
            } else if (j < constructors.length &&
                    met[0].matches(".*(" + constructors[j].getDeclaringClass().getSimpleName() + ")\\s*[(]{1}.*")) {
                this.flag2 = true;
                methodName = CONSTRUCTOR_MARK + constructors[j].getDeclaringClass().getSimpleName();
            }
        }
        this.flag2 = false;
        file = new File(path, methodName + ".txt"); //создаем файл
        int index = 2;
        boolean exist = false;
        try {
           while (!exist && file.exists()) { //если метод был перегужен
               if (!Files.readAllLines(file.toPath()).get(0).equals(met[0])) {
                   System.out.println("Это перегруженный метод, в название будет добавлен индекс метода.");
                   file = new File(path, methodName + index + EXTENSION_TXT);

                   index++;
               } else {
                   exist = true;
               }
           }
           file.createNewFile();
           FileChannel fc = new FileOutputStream(file).getChannel();
           fc.write(ByteBuffer.wrap(resMethod.getBytes()));
           fc.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private String getConcreteMethod(String methods,int choice){ //полная реализация конкретного метода
        this.choice = choice;
        String method  = Arrays.stream(methods.split("\n"))  //вытаскиваем имя метода
                .filter(n -> n.contains(choice + "  "))
                .reduce("",(acc,n) -> acc + n);
        if(method.contains("{  ")){ //костыль для стандартного добавления }
            method = Arrays.stream(method.split("\\s*[}]"))
                    .filter(n -> !(n.equals("")))
                    .reduce("",(acc,n) -> acc + n);
        }
        else{
            method = method.replace("}","");
        }
        method = method.replace(choice + "","");
        return getResMethod(method) ;
    }

    private String getResMethod(String method) {
        final boolean[] finalFlag = {false}; //главный флаг
        final boolean[] flagDop = {false}; //доп.флаг
        final int[] i = {0};
        String resMethod = "";
        try {
            String finalMethod = method;
            resMethod = Files.lines(directory.toPath())  // получаем метод с телом
                    .filter(n -> {
                        if(!finalFlag[0]){
                            if(flagDop[0]){
                                return false;
                            }
                            else {
                                finalFlag[0] = n.contains(finalMethod); //есть ли реализация метода
                                if (finalFlag[0] && n.contains("}")) { //если true и есть }
                                    finalFlag[0] = false; //значит метод в одну строку
                                    flagDop[0] = true;
                                    return true;
                                }
                                if(finalFlag[0]) i[0]++;
                                flagDop[0] = finalFlag[0];
                            }
                        }
                        else{//если главный тру,значит зашли в метод,отсчитываем теперь скобки
                            long count1 = Arrays.stream(n.split("")).filter(k -> k.equals("{")).count();
                            long count2 = Arrays.stream(n.split("")).filter(k -> (k.equals("}"))).count();
                            if(count1 == count2){
                                return true;
                            }
                            if(count1 > count2) {
                                i[0] += count1 - count2;
                            }else {
                                i[0] -= count2 - count1;
                            }
                            if(i[0] == 0){
                                flagDop[0] = true;
                                finalFlag[0] = false;
                                return true;
                            }

                        }
                        return flagDop[0];
                    })
                    .reduce("",(acc,str) -> acc + "\n" + str);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return resMethod;
    } // получить полную реализацию метода(главная часть)
}

