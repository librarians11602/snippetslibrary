import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by user on 07.05.2017.
 */
public class FileWorker {
    //private static String METHOD_SIGNATURE_REGEX = "\\s*((public)||(private)||(protected)){1}.*[(]{1}[A-z0-9,]*\\s*[)]{1}.*";
    private static final String CLIENTS_DIRECTORY_NAME = "\\ClientsFiles\\";
    private static final String CLIENTS_DIRECTORY_NAME_1 = "ClientsFiles/";
    private static final String CLIENTS_DIRECTORY_NAME_2 = "ClientsFiles";
    private static final String CLIENTS_FILES_DIRECTORY_PATH = "src/ClientsFiles";
    private static final String METHODS_DIRECTORY = "/Methods";
    private static final String CLASS_DIRECTORY = "/Class";
    private static final String ABSTRACT_STRUCTURE_DIRECTORY = "/AbstractClass";
    private static final String LIBRARY_MARK = "Library";
    private static final String ABSTRACT_STRUCTURE_MARK = "Abstract";
    private static final String PROJECT_ROOT_DIRECTORY = "src";
    private static final String PROJECT_ROOT_DIRECTORY_1 = "src/";
    private static final String EXTENSION_TXT = ".txt";
    private static final String EXTENSION_JAVA = ".java";
    private static final String ACCESS_MODIFIER_PUBLIC = "public";
    private static final String ACCESS_MODIFIER_PRIVATE = "private";
    private static final String ACCESS_MODIFIER_PROTECTED = "protected";
    private static final String CLASS_MODIFIER = "class";

    private  int i = 1; // счетчик для нумерации директорий
    private  int choice = 1; // выбор на данном шаге
    private  Scanner sc = new Scanner(System.in);
    private  String path = ""; // путь для файла
    private  String className = ""; // имя файла java
    private  boolean javaFlag = true; //является ли файл java(для add)
    private  boolean flag = false; // работай,пока не найдешь файл
    private  boolean choiceIllegal = true; // для файла
    private  boolean choiceIllegalDir; //для директории
    private  File file = null;
    private  File directory;

    public void workWithFile(){
        directory = findFile();// ищем файл
            //System.out.println(directory.getName());
            if(directory.getPath().contains(CLIENTS_DIRECTORY_NAME)){ // все клиент.файлы - txt
                workWithTXTFile();
            }
            else {
                workWithJavaFile();
            }
    } // работа с файлом(поиск по библиотеке
    private void workWithTXTFile(){
        javaFlag = false;
        try {
            System.out.println(Files.lines(directory.toPath()).reduce("",(acc,n) -> acc + n + "\n")); // если txt ,то просто вывод
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
    private void workWithJavaFile(){
        try{
            Class ourClass; // наш класс для рефлексии
            boolean choiceIllegal; // неверный выбор
            String path = directory.getPath(); // путь выбранного файла java
            path = path.replace('\\', '.').replace("src.", "").replace(".java", "");
            ourClass = Class.forName(path);// ищем класс по имени
            choiceIllegal = true; // пока значение неверно
            while (choiceIllegal) {
                try {
                    System.out.println("==================================================================================");
                    System.out.println("Выберите,что вы хотите получить : \n1: Метод \n2: Полная реализация \n3: Обобщенная структура");
                    System.out.print("Выбор: ");
                    choice = sc.nextInt();
                    System.out.println("==================================================================================");
                    switch (choice) {
                        case 1:
                            writeMethod(ourClass, false);
                            choiceIllegal = false;
                            break;
                        case 2:
                            writeAllClass();
                            choiceIllegal = false;
                            break;
                        case 3:
                            writeAbstractStructure(ourClass);
                            choiceIllegal = false;
                            break;
                        default:
                            throw new IllegalStateException();
                    }
                    // TODO формула для вычисления времени задержки
                    if (Thread.currentThread().getName().equals("main")) Thread.currentThread().sleep(1000);
                } catch (IllegalStateException ex) {
                    System.out.println("Значение введено неправильно.\nВведите заново значение.");
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public File findFile() { // поиск файла
        //directory = new File("src/AlgoritmMusin/May/ProjectLibrarySnippet");//исходная директория
        //path = "src/AlgoritmMusin/May/ProjectLibrarySnippet/"; //путь будет строится
        directory = new File(PROJECT_ROOT_DIRECTORY_1);//исходная директория
        path = PROJECT_ROOT_DIRECTORY_1; //путь будет строится
        while (!flag) {
            i = 1;
            if(choice == 0) //если мы вернулись назад
                choiceIllegal = true;
            if (directory.isDirectory()) {
                List<File> files = Arrays.stream(directory.listFiles()) // ищем директории
                        .filter(File::isDirectory)
                        .collect(Collectors.toList());
                if (files.size() != 0) { //если размер не равен нулю
                    choiceIllegalDir = true; //обновляем на каждой итерациии
                    while (choiceIllegalDir) {
                        files.stream().map(File::getName).forEach(n -> {
                            System.out.println(i + " : " + n + "  ");
                            i++;
                        });
                       goThroughDirectory(files);
                    }
                }
                else goThroughFile();
            }
        }
        return directory;
    } // найти файл
    private void goThroughDirectory(List<File> files){
        try {
            System.out.println("Выберите директорию из предложенных или вернитесь назад (0): ");
            System.out.print("Выбор: ");
            choice = sc.nextInt();
            System.out.println("==================================================================================");
            if (choice > 0 && choice <= files.size()) {
                directory = files.get(choice - 1);
                if(directory.getName().equals(LIBRARY_MARK)){ //пути меняются в начале
                    path += CLIENTS_DIRECTORY_NAME_1;
                }
                else {
                    path += directory.getName() + "/"; //в остальных случаях структура одинакова
                    file = new File(path);
                    file.mkdir();
                }
                choiceIllegalDir = false; //все нормально,директория создана
            } else if (choice == 0) { // возвращение назад
                if (!directory.getName().equals(PROJECT_ROOT_DIRECTORY)) { //если директория не корень проекта
                //if (!directory.getName().equals("ProjectLibrarySnippet")) { //если директория не корень проекта
                    if(directory.getName().equals(LIBRARY_MARK)){ //помним о единственном различиии в пути
                        path = path.replace(CLIENTS_DIRECTORY_NAME_1,"");
                    }
                    else{
                        path = path.replace(directory.getName() + "/", ""); //удаление пути
                    }
                    directory = directory.getParentFile();
                    choiceIllegalDir = false; //директория найдена
                } else {
                    System.out.println("Вы уже находитесь в корневой директории библиотеки. ");
                    choiceIllegalDir = false;
                }
            } else {
                throw new IllegalStateException();
            }
        } catch (IllegalStateException ex) {
            System.out.println("Значение введено неправильно.\nВведите значение заново.");
            i = 1;
        }
    } //ходим по директориям
    private void goThroughFile(){
        //количество директорий в данной  равно 0
        List<File> files = Arrays.stream(directory.listFiles()) //директория с файлами(сниппетами)
                .filter(n -> !(n.isDirectory()))
                .collect(Collectors.toList());
        if(files.size()==0){//если нет и обычных файлов,искать нечего
            System.out.println("Папка пуста. Вы вернетесь назад.");
            path = path.replace(directory.getName() + "/","");
            directory = directory.getParentFile();
        }
        else { //файлы есть
            files.stream().map(File::getName).forEach(n -> {
                System.out.println(i + " : " + n + "  ");
                i++;
            });
            while (choiceIllegal) { // проверка для файлов
                try {
                    System.out.println("Выберите файл из предложенных или вернитесь назад (0): ");
                    System.out.print("Выбор: ");
                    choice = sc.nextInt();
                    System.out.println("==================================================================================");
                    if (choice > 0 && choice <= files.size()) {
                        flag = true;
                        directory = files.get(choice - 1);
                        if (javaFlag) { // если файл java
                            className = directory.getName().replace(".java", "");
                            path += className;
                            file = new File(path);
                            file.mkdir();
                        }
                        choiceIllegal = false;
                    } else if (choice == 0) { //возвращение назад,к директориям
                        path = path.replace(directory.getName() + "/", "");
                        directory = directory.getParentFile();
                        choiceIllegal = false;
                    } else throw new IllegalStateException();
                } catch (IllegalStateException ex) {
                    System.out.println("Значение введено неправильно. \nВведите заново значение. ");
                }
            }
        }

    } //ходим по файлам

    private void writeMethod(Class ourClass,boolean dopFlag){//запись метода
       flag = false;
        while(!flag){
            try {
                // TODO регулярное выражение
                //Pattern comment = Pattern.compile("\\s+.+[^(;||.||/||+||=||-||>||<)*][()]{1}(\\s*||[A-z0-9]*)[^(;||.||/||+||=||-||>||<)*]"); // ищем конструкторы и методы
                //Pattern comment = Pattern.compile(".*[.;=><-+\/*]+.*");
                Pattern comment = Pattern.compile(".*");
                String methods = getMethodsSignature(comment); // получаем сигнатуры методов
                int count = this.i;
                String numbersMethod = "";
                if(!dopFlag) { // ищем файл(false) или добавляем
                    System.out.println(methods);
                    System.out.println("Выберите метод (цифру) или несколько методов через запятую : ");
                    System.out.print("Выбор: ");
                    sc.nextLine();
                    System.out.println("==================================================================================");
                    numbersMethod = sc.nextLine();
                }
                else{
                    for(int i = 1;i < count ;i++ ) {
                        numbersMethod += i + ",";
                    }
                    numbersMethod += count; //запишем все
                }
                findAllMethods(numbersMethod,count,methods,ourClass);
                path = path.replace(METHODS_DIRECTORY,"");// для разбивки на методы ,класс и абст. класс.
                flag = true;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    } // записать методы
    private void writeAllClass(){ //запись всего класса
        try {
            File file;
            path += CLASS_DIRECTORY;
            file = new File(path);
            file.mkdir();
            String allClass =  Files.lines(directory.toPath()).reduce((acc,str) -> acc + "\n" + str).get();
            //System.out.println(allClass); // вывод всего класса
            file = new File(path,className + EXTENSION_TXT);
            file.createNewFile();
            FileChannel fc = new FileOutputStream(file).getChannel();
            fc.write(ByteBuffer.wrap(allClass.getBytes()));
            fc.close();
            path = path.replace(CLASS_DIRECTORY,"");
            //System.out.println("Хотите изменить файл?");
            //sc.nextLine();
            //getChoice();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private   void writeAbstractStructure(Class ourClass) {
        try {
            //Pattern comment = Pattern.compile("\\s+.+([()]{1}\\s*[{]\\s*(\\s||[}]))||(.*(class){1}.*)");
            String res = Files.lines(directory.toPath())         //добавляет конструкторы,методы и сам класс
                    .filter(n ->
                            (n.contains(ACCESS_MODIFIER_PRIVATE) ||
                                    n.contains(ACCESS_MODIFIER_PROTECTED) ||
                                    n.contains(ACCESS_MODIFIER_PUBLIC)) ||
                                    n.contains(CLASS_MODIFIER))
                    .reduce("", (acc, el) -> {
                        if (el.contains("{") && !el.contains("}") && !el.contains(CLASS_MODIFIER)) {
                            el += "}";
                        }
                        return acc + el + "\n";
                    });

            Field[] fields = ourClass.getDeclaredFields(); // поля через рефлексию
            String poles = Arrays.stream(fields)
                    .map(n -> "    " + Modifier.toString(n.getModifiers()) + " " +
                            n.getType().getSimpleName() + " " + n.getName() + " ; \n")
                    .reduce("", (acc, n) -> acc + n);

            res = Arrays.stream(res.split("\n"))
                    .map(n -> {     // теперь поля надо вставить после класса,но до методов
                        if (n.contains(CLASS_MODIFIER) && n.contains("{") && !poles.equals("")) {
                            return n + "\n\n" + poles + "\n";
                        }
                        if (n.equals("")) {
                            return "";
                        }
                        return n + "\n\n";
                    })
                    .reduce((acc, n) -> acc + n).get() + "}";

            // System.out.println(res); // вывод структуры
            File file;
            path += ABSTRACT_STRUCTURE_DIRECTORY;
            file = new File(path);
            file.mkdir();
            file = new File(path,ABSTRACT_STRUCTURE_MARK + className + EXTENSION_TXT);
            file.createNewFile();
            FileChannel fc = new FileOutputStream(file).getChannel();
            fc.write(ByteBuffer.wrap(res.getBytes()));
            fc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void findAllMethods(String numbersMethod,int count,String methods,Class ourClass) throws IOException{
        Method[] methodsNames = ourClass.getDeclaredMethods(); //методы
        Constructor[] constructors = ourClass.getDeclaredConstructors(); //конструкторы
        String[] choices = numbersMethod.split("\\s*,\\s*");// наши выборы методов
        File file;
        path += METHODS_DIRECTORY; //создаем папку
        file = new File(path);
        file.mkdir();
        boolean flag2 = false; // флаг для метода(пока не найдем совпадение)
        ExecutorService ex = Executors.newFixedThreadPool(count);

        for(int i = 0;i<choices.length;i++) {
            choice = Integer.valueOf(choices[i]);
            FileHelper currentFileHelper =
                  new FileHelper(choice,constructors,methodsNames,flag2,count,path,methods,file,directory);
            ex.execute(currentFileHelper);
        }
         ex.shutdown();
    } //поиск методов
    private String getMethodsSignature(Pattern comment){ //получаем сигнатуры методов
        String methods = "";
        i = 1;
        final boolean[] methodFlag = new boolean[1];
        final boolean[] k = {false};
        try {
            methods = Files.lines(directory.toPath())
                    .filter(n -> {
                        if(comment.matcher(n).matches()){
                          k[0] = true;
                        }
                     //   if(k[0] && (!(n.contains("for")||n.contains("while")||n.contains("if")
                     //           || n.contains("switch")||n.contains("catch") || n.contains("try"))
                     //           || (n.contains("public") || n.contains("private")) || n.contains("protected"))){
                     //       methodFlag[0] = true;
                     //       k[0] = false;
                     //   }
                        if(k[0] &&
                                (n.contains(ACCESS_MODIFIER_PRIVATE) ||
                                        n.contains(ACCESS_MODIFIER_PROTECTED) ||
                                        n.contains(ACCESS_MODIFIER_PUBLIC)) &&
                                n.contains("(")){
                              methodFlag[0] = true;
                              k[0] = false;
                          }
                        else{
                            k[0] = false;
                            methodFlag[0] = false;

                        }
                        return methodFlag[0];
                    })
                    .reduce("",(acc,el) -> {
                        if(!(el.contains("{"))){
                            el += "{";
                        }
                        else if(el.contains("}")){
                            el = el.replace('}',' ');
                        }
                        return acc + i++ + el + "}" + "\n";
                    });

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        i--;
        return methods;
    } // получить сигнатуры методов

    public void addFile(File file) { // добавить файл
        try {
            this.file  = file;
            if (file.getPath().endsWith(EXTENSION_JAVA)) { // только java
                //System.out.println(file.getName());
                directory = new File(CLIENTS_FILES_DIRECTORY_PATH);
                flag = false; // также как в поиске
                path = "";
                choiceIllegal = false; //правильный ли ввод
                boolean choiceFalse = false; //чтобы выбрать любую директорию в библиотеке
                try {
                while (!flag) {// пока не загрузим файл
                    if (directory.isDirectory()) {
                        List<File> files = Arrays.stream(directory.listFiles())
                                .filter(File::isDirectory)
                                .collect(Collectors.toList());
                        i = 1;
                        if (files.size() != 0 && !choiceFalse) {
                            choiceFalse = chooseDirectory(files,choiceFalse);
                        }else {//как обычно ,нет папок,только файлы
                            choiceFalse = chooseFile(choiceFalse);
                        }
                    }
                }

                } catch (IllegalStateException ex) {
                    System.out.println("Значение введено неправильно.\nВведите заново значение.");
                    i = 1;
                }
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex){
            System.out.println("Недопустимое расширение файла. Добавьте файл с расширением .java");
        }
    }  //добавить файл
    private void loadFile(){
        flag = true;
        className = file.getName().replace(EXTENSION_JAVA, "");
        File fileNew = new File(directory, className);
        if (fileNew.exists()) {
            System.out.println("==================================================================================");
            System.out.println("Файл с таким именем уже существует. \n Хотите перезаписать его? " +
                    "\n1: да \n2: нет \n3: переименовать файл");
            System.out.print("Выбор: ");
            choice = sc.nextInt();
            System.out.println("==================================================================================");
            choiceIllegal = true;
            while (choiceIllegal) {
                try {
                    switch (choice) {
                        case 1:
                            System.out.println(fileNew.delete());
                            path += className;
                            writeToFile(file,fileNew);
                            choiceIllegal = false;
                            break;
                        case 2:
                            System.out.println("Новый файл не добавлен.");
                            choiceIllegal = false;
                            break;
                        case 3:
                            System.out.println("Введите новое имя: ");
                            sc.nextLine();
                            String name = sc.nextLine();
                            System.out.println();
                            fileNew = new File(directory, name);
                            fileNew.mkdir();
                            path += className;
                            writeToFile(file,fileNew);
                            choiceIllegal = false;
                            break;
                        default:
                            throw new IllegalStateException();
                    }
                } catch (IllegalStateException ex) {
                    System.out.println("Значение введено неправильно.\nВведите заново значение.");
                    i = 1;
                }
            }
        } else { //такого файла нет ,просто загружаем
            fileNew.mkdir();
            path += className;
            writeToFile(file, fileNew);
        }
    }

    private boolean chooseDirectory(List<File> files,boolean choiceFalse) {
        try {
            System.out.println("Доступные директории: ");
            files.stream().map(File::getName).forEach(n -> {
                System.out.println(i + " : " + n + "  ");
                i++;
            });
            System.out.println("==================================================================================");
            System.out.println("\n1 : Выбрать директорию из предложенных  \n2 : Cоздать новую   \n3 : Вернуться назад" +
                    "\n4 : Загрузить файл в " + directory.getPath());
            System.out.print("Выбор: ");
            choice = sc.nextInt();
            System.out.println("==================================================================================");
            choiceIllegal = true;
            switch (choice) {
                case 1:
                    while (choiceIllegal) {
                        try {
                            if (files.size() == 1) { //если всего одна папка,то выбирать нечего
                                directory = files.get(choice - 1);
                                path += directory.getName() + "/";
                                choiceIllegal = false;
                            } else {//выбираем
                                i = 1;
                                files.stream().map(File::getName).forEach(n -> {
                                    System.out.println(i + " : " + n + "  ");
                                    i++;
                                });
                                System.out.println("Введите номер директории.");
                                System.out.print("Выбор: ");
                                choice = sc.nextInt();
                                System.out.println("==================================================================================");
                                if (choice > 0 && choice <= files.size()) {
                                    directory = files.get(choice - 1);
                                    path += directory.getName() + "/";
                                    choiceIllegal = false;
                                } else {
                                    throw new IllegalStateException();
                                }
                            }
                        } catch (IllegalStateException ex) {
                            System.out.println("Значение введено неправильно.\nВведите заново значение.");
                            i = 1;
                        }
                    }
                    break;
                case 2:
                    sc.nextLine();
                    System.out.println("Введите название: ");
                    String name = sc.nextLine();
                    directory = new File(directory, name);
                    directory.mkdir();
                    path += directory.getName() + "/";
                    break;
                case 3:
                    if (directory.getName().equals(CLIENTS_DIRECTORY_NAME_2)) {
                        System.out.println("Вы уже находитесь в корневой директории.");
                    } else {
                        path = path.replace(directory.getName() + "/", "");
                        directory = directory.getParentFile();
                    }
                    break;
                case 4:
                    if (directory.getName().equals(CLIENTS_DIRECTORY_NAME_2)) {
                        System.out.println("Нельзя создать директорию в корневой папке.");
                    } else {
                        choiceFalse = true;
                    }
                    break;
                default:
                    throw new IllegalStateException();
            }
        } catch (IllegalStateException ex) {
            System.out.println("Значение введено неправильно.\nВведите заново значение.");
            i = 1;
        }
        return choiceFalse;
    }
    private boolean chooseFile(boolean choiceFalse) {
        if (!choiceFalse) {
            System.out.println("==================================================================================");
            System.out.println("1: Загрузить файл в : " + directory.getPath() + "\n2: Создать новую директорию ?" +
                    "\n3: Вернуться назад ");
            System.out.print("Выбор: ");
            choice = sc.nextInt();
            System.out.println("==================================================================================");
            choiceFalse = false;
        }
        else{//наш выбор уже известен,мы хотим загрузить,несмотря на наличие файлов
            choice = 1;
        }
        switch (choice) {
            case 1:            //загрузить файл
                loadFile();
                break;

            case 2:  //создать директорию
                System.out.println("Введите название каталога: ");
                sc.nextLine();
                String name = sc.nextLine();
                directory = new File(directory, name);
                boolean flagJ = directory.mkdir();
                System.out.println(flagJ);
                path += directory.getName() + "/";
                break;

            case 3: //вернуться назад
                if (directory.getName().equals(CLIENTS_DIRECTORY_NAME_2)) {
                    System.out.println("Вы уже находитесь в корневой директории.");
                } else {
                    path = path.replace(directory.getName() + "/", "");
                    directory = directory.getParentFile();
                }
                break;
            default:
                throw new IllegalStateException();
        }
        return choiceFalse;
    }

    private void writeToFile(File filePast,File fileNew){ //запись файла
        try {
            String pathClass = filePast.getPath();
            //pathClass = pathClass.replace('\\', '.').replace("src.", "").replace(".java", "");
            pathClass = pathClass.replace('\\', '.').replace("src.", "").replace(EXTENSION_JAVA, "");
            Class ourClass = Class.forName(pathClass); // ищем класс по имени
            path = fileNew.getPath().replace(EXTENSION_JAVA, "");
            directory = filePast;
            //разбиваем на методы ,класс и абс.класс и записываем
            writeMethod(ourClass, true);
            writeAllClass();
            writeAbstractStructure(ourClass);
            System.out.println("Файл " + filePast.getAbsolutePath() + " обработан.\n" +
                    "Результат в директории: " + fileNew.getAbsolutePath());    // сообщение о успешном завершении
        } catch (ClassNotFoundException e) {
            System.out.println("Класс не найден. " + e.getMessage());

        }
    }
    public void setStandard(){
        i = 1; // счетчик для нумерации директорий
        choice = 1; // выбор на данном шаге
        path = ""; // путь для файла
        className = ""; // имя файла java
        javaFlag = true; //является ли файл java(для add)
        flag = false; // работай,пока не найдешь файл
        choiceIllegal = true; // для файла
        choiceIllegalDir = false; //для директории
        file = null;
        directory = null;
    }
}